module gitlab.com/tploss/markdown

go 1.21.3

require (
	github.com/alvaroloes/enumer v1.1.2
	github.com/yuin/goldmark v1.6.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/pascaldekloe/name v0.0.0-20180628100202-0fd16699aae1 // indirect
	golang.org/x/tools v0.0.0-20190524210228-3d17549cdc6b // indirect
)
