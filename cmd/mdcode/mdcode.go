// mdcode is a reference implementation for a consumer of gitlab.com/tploss/markdown.
// It offers testing and verification opportunity for the complete markdown package functionality.
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log/slog"
	"os"
	"strings"

	"gitlab.com/tploss/markdown"

	"gopkg.in/yaml.v3"
)

var log *slog.Logger

func init() {
	log = slog.New(slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{Level: slog.LevelInfo}))
	slog.SetDefault(log)
}

func main() {
	markdown.Log = log
	source, err := os.ReadFile("testdata/demo.md")
	if err != nil {
		panic(err)
	}
	cs, err := markdown.ExtractCodeSections(source)
	if err != nil {
		panic(err)
	}

	var format string
	flag.StringVar(&format, "format", "json", "Define output format")
	flag.Parse()

	switch format {
	case "json":
		enc := json.NewEncoder(os.Stdout)
		enc.SetEscapeHTML(false)
		enc.SetIndent("", "  ")
		if err = enc.Encode(cs); err != nil {
			panic(err)
		}
	case "yaml":
		d, err := yaml.Marshal(&cs)
		if err != nil {
			panic(err)
		}
		i, err := fmt.Fprint(os.Stdout, string(d))
		if err != nil || i != len(d) {
			panic(fmt.Errorf("Unable to write %d bytes, wrote %d, error: %w", len(d), i, err))
		}
	case "plain":
		err := writePlain(os.Stdout, cs)
		if err != nil {
			panic(err)
		}
	case "fzf":
		err := writeFZF(os.Stdout, cs)
		if err != nil {
			panic(err)
		}
	}
}

func writePlain(w io.Writer, cs markdown.CodeSections) error {
	var sb strings.Builder
	for _, c := range cs {
		_, _ = sb.WriteString(fmt.Sprintf("%s\n", c.String()))
	}
	_, err := w.Write([]byte(sb.String()))
	return err
}

func writeFZF(w io.Writer, cs markdown.CodeSections) error {
	var sb strings.Builder
	for _, c := range cs {
		lang := c.Language
		if lang == "" {
			lang = "no language"
		}
		desc := c.Description
		if desc == "" {
			// inline code will not have a description, then use text
			desc = c.Text
		}
		_, _ = sb.WriteString(fmt.Sprintf("%s:%s\n", lang, desc))
	}
	_, err := w.Write([]byte(sb.String()))
	return err
}
