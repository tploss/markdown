package markdown

import (
	"bytes"
	"encoding/json"
	"os"
	"testing"
)

const demoJSON = `[
  {
    "description": "",
    "kind": "inline code",
    "pos": {
      "start_line": 3,
      "end_line": 3
    },
    "code": "inline code"
  },
  {
    "description": "Run these 2 lines in your shell (bash & zsh supported)",
    "kind": "code block with language",
    "pos": {
      "start_line": 6,
      "end_line": 10
    },
    "language": "language",
    "code": "This is a multi\nline code block\n"
  },
  {
    "description": "",
    "kind": "inline code",
    "pos": {
      "start_line": 11,
      "end_line": 11
    },
    "code": "loadenv"
  },
  {
    "description": "",
    "kind": "inline code",
    "pos": {
      "start_line": 11,
      "end_line": 11
    },
    "code": "unsetenv"
  },
  {
    "description": "",
    "kind": "inline code",
    "pos": {
      "start_line": 11,
      "end_line": 11
    },
    "code": ".env"
  },
  {
    "description": "Heading as description",
    "kind": "code block with language",
    "pos": {
      "start_line": 13,
      "end_line": 18
    },
    "language": "language 2",
    "code": "Multi\nline code block\nwith heading as description\n"
  },
  {
    "description": "",
    "kind": "inline code",
    "pos": {
      "start_line": 22,
      "end_line": 26
    },
    "code": "deactivate"
  },
  {
    "description": "heading for code block",
    "kind": "code block without language",
    "pos": {
      "start_line": 28,
      "end_line": 31
    },
    "code": "This is a\ncode block that\nis indented with 4 spaces with a heading above\n"
  },
  {
    "description": "",
    "kind": "inline code",
    "pos": {
      "start_line": 33,
      "end_line": 33
    },
    "code": "util.py"
  },
  {
    "description": "",
    "kind": "inline code",
    "pos": {
      "start_line": 34,
      "end_line": 35
    },
    "code": "util.py"
  },
  {
    "description": "",
    "kind": "inline code",
    "pos": {
      "start_line": 34,
      "end_line": 35
    },
    "code": "python"
  },
  {
    "description": "To import this library in the application specific ` + "`python`" + ` scripts add the following 3 lines.",
    "kind": "code block without language",
    "pos": {
      "start_line": 35,
      "end_line": 39
    },
    "code": "This is a\ncode block that\nis indented with 4 spaces\n"
  },
  {
    "description": "",
    "kind": "inline code",
    "pos": {
      "start_line": 45,
      "end_line": 45
    },
    "code": "inline code"
  },
  {
    "description": "First code snippet in list",
    "kind": "inline code",
    "pos": {
      "start_line": 49,
      "end_line": 49
    },
    "code": "list code snippet 1"
  },
  {
    "description": "Second code snippet in list",
    "kind": "inline code",
    "pos": {
      "start_line": 50,
      "end_line": 51
    },
    "code": "list code snippet 2"
  },
  {
    "description": "Third code snippet in list",
    "kind": "inline code",
    "pos": {
      "start_line": 52,
      "end_line": 52
    },
    "code": "list code snippet 3"
  },
  {
    "description": "",
    "kind": "inline code",
    "pos": {
      "start_line": 54,
      "end_line": 54
    },
    "code": "final inline code"
  }
]
`

func TestExtractCodeSections(t *testing.T) {
	demoData, err := os.ReadFile("testdata/demo.md")
	if err != nil {
		t.Fatalf("Unable to open demoData: %s", err)
	}
	tests := []struct {
		name     string
		source   []byte
		wantJSON string
		wantErr  bool
	}{
		{name: "demo.md", source: demoData, wantJSON: demoJSON, wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ExtractCodeSections(tt.source)

			var buf bytes.Buffer
			enc := json.NewEncoder(&buf)
			enc.SetEscapeHTML(false)
			enc.SetIndent("", "  ")
			if encErr := enc.Encode(got); encErr != nil {
				t.Fatalf("JSON encode failed for %s: %s", tt.name, encErr)
			}

			if (err != nil) != tt.wantErr {
				t.Errorf("ExtractCodeSections() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if buf.String() != tt.wantJSON {
				t.Errorf("ExtractCodeSections() = %v, want %v", got, tt.wantJSON)
			}
		})
	}
}
