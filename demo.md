# ADE Scripts

1. Use web UI of application instances to generate tokens. Configure them in `inline code` as explained below.

### Usage in manual commands
Run these 2 lines in your shell (bash & zsh supported):
```language
This is a multi
line code block
```
Now you can run `loadenv` and `unsetenv` to load/remove the `.env` values to/from your environment.

### Dependencies
To install dependencies needed for the respective script switch into the root folder of the repository and run the following commands:
> multiple lines
> that start
> with '> '
> to create a quote
To leave the virtual environment just run the `deactivate` command.

### **`util.py`**
Reusable functions can be placed in the repo level `util.py` file.
To import this library in the application specific python scripts add the following 3 lines.

    This is a 
    code block that
    is indented with 4 spaces
A normal sentence.

### The scripts
Check the application specific READMEs in the sub directories for python scripts related to those applications.

Expecting one more _`inline code`_ section.
