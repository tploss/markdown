# markdown

This project mainly re-uses [github.com/yuin/goldmark](https://pkg.go.dev/github.com/yuin/goldmark) and focuses on extracting pieces of code from markdown files.

The goal is to select and run code snippets from a snippet collection maintained in markdown.
