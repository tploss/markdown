/*
Package markdown implements Code snippet/section extraction from markdown sources.
As it uses [github.com/yuin/goldmark] it is compliant with [CommonMark 0.30].

It can deal with all 3 different types of code sections defined in CommonMark:
  - [inline code spans], referred to as [Kind] [Inline]
  - [indented code blocks], referred to as [Kind] [Block]
  - [fenced code blocks], referred to as [Kind] [BlockWithLang]

Check the example to see what data is collected for each [Code] in [CodeSections].

[CommonMark 0.30]: https://spec.commonmark.org/0.30/
[inline code spans]: https://spec.commonmark.org/0.30/#code-spans
[indented code blocks]: https://spec.commonmark.org/0.30/#indented-code-blocks
[fenced code blocks]: https://spec.commonmark.org/0.30/#fenced-code-blocks
*/
package markdown
