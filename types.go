package markdown

import (
	"fmt"
	"strings"
)

//go:generate go run github.com/alvaroloes/enumer -linecomment -type Kind -json -yaml

// A Kind is used to identify the type of [Code] section.
// See package doc for references to the CommonMark spec.
// The methods are managed by [github.com/alvaroloes/enumer].
type Kind int

const (
	BlockWithLang Kind = iota // code block with language
	Block                     // code block without language
	Inline                    // inline code
)

// A Position contains the Start and End line of a [Code] section within the markdown source.
type Position struct {
	Start int `json:"start_line" yaml:"start_line"`
	End   int `json:"end_line" yaml:"end_line"`
}

// CodeSections contain multiple [Code] sections.
type CodeSections []*Code

// [fmt.Stringer] implementation.
func (cs CodeSections) String() string {
	var s strings.Builder
	for _, c := range cs {
		_, _ = s.WriteString(c.String())
	}
	return s.String()
}

// Code describes a Code section extracted from a markdown source.
type Code struct {
	// Description is the closest text to the code snippet which describes the snippet.
	Description string `json:"description" yaml:"description"`
	// [Kind] provides the type of markdown code section.
	Kind Kind `json:"kind" yaml:"kind"`
	// [Position] in the source file, includes the Description source.
	Position Position `json:"pos" yaml:"pos"`
	// Language contains the language defined for the code section.
	// If no language was defined it is "".
	Language string `json:"language,omitempty" yaml:"language,omitempty"`
	// Text contains the complete text of the code section.
	// This can be multiple lines and will always end with "\n".
	Text string `json:"code" yaml:"code"`
}

// [fmt.Stringer] implementation.
func (c Code) String() string {
	switch c.Kind {
	case BlockWithLang:
		return fmt.Sprintf("%d-%d\t%s %s: %q", c.Position.Start, c.Position.End, c.Kind, c.Language, c.Text)
	case Block:
		return fmt.Sprintf("%d-%d\t%s: %q", c.Position.Start, c.Position.End, c.Kind, c.Text)
	case Inline:
		return fmt.Sprintf("%d-%d\t%s: %q", c.Position.Start, c.Position.End, c.Kind, c.Text)
	}
	panic(fmt.Sprintf("Unknown `Code.Kind`: %s", c.Kind.String()))
}
