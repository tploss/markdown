package markdown

import (
	"bytes"
	"log/slog"
	"slices"
	"strings"
	"unicode/utf8"

	"github.com/yuin/goldmark"
	"github.com/yuin/goldmark/ast"
	"github.com/yuin/goldmark/text"
)

// Log is the logger used within [markdown] and can be overwritten by package users to achieve alternative
// logging behavior such as different outputs or a different level. Uses [log/slog] for structured logs.
var Log = slog.Default()

// ExtractCodeSections parses and walks the provided markdown source to extract all [Code] sections within.
func ExtractCodeSections(source []byte) (CodeSections, error) {
	// find line breaks for source positions
	lineBreaks := getLineBreakIndexes(source)

	// parse the markdown
	cs := make(CodeSections, 0)
	root := goldmark.DefaultParser().Parse(text.NewReader(source))
	err := ast.Walk(root, func(node ast.Node, entering bool) (ast.WalkStatus, error) {
		if entering {
			// we only want to visit each `ast.Node` once
			return ast.WalkContinue, nil
		}
		var c *Code
		switch node.Kind() {
		case ast.KindFencedCodeBlock:
			c = getFencedCodeBlockCode(node, source, lineBreaks)
			Log.Debug("found code block with language", "language", c.Language, "text", c.Text)

		case ast.KindCodeBlock:
			c = getCodeBlockCode(node, source, lineBreaks)
			Log.Debug("found code block without language", "text", c.Text)

		case ast.KindCodeSpan:
			c = getCodeSpanCode(node, source, lineBreaks)
			Log.Debug("found inline code", "Code", &c)

		}
		if c != nil {
			cs = append(cs, c)
		}
		return ast.WalkContinue, nil
	})
	if err != nil {
		return nil, err
	}
	return cs, nil
}

// getLineBreakIndexes reports all indexes of newline characters in the source.
// This can be used to find out line numbers of specific source indexes.
// The implementation is heavily inspired by `bytes.Runes()` but does not use
// it to not go through the same data twice.
func getLineBreakIndexes(s []byte) []int {
	// enter 1 item to offset by one for human readable line numbers
	lb := make([]int, 1)
	lb[0] = 0

	i := 0
	for len(s) > 0 {
		r, l := utf8.DecodeRune(s)
		if r == '\n' {
			lb = append(lb, i)
		}
		i++
		s = s[l:]
	}
	return lb
}

// getPos finds the start and end index of the given ast.Node or its closest possible
// parent in the source. `goldmark` does not provide this info for all ast.NodeKinds.
func getPos(node ast.Node) (start, end int) {
	if node.Kind() == ast.KindText {
		// ast.Text is the only type that has a Segment field
		n := node.(*ast.Text)
		return n.Segment.Start, n.Segment.Stop
	}
	// for all other node types we have to check whether they provide us Lines() information
	// otherwise we check their parent node
	defer func() {
		if recover() != nil {
			start, end = getPos(node.Parent())
		}
	}()
	l := node.Lines()
	start = l.At(0).Start
	end = l.At(l.Len() - 1).Stop
	return
}

// getTextAndPos simplifies extracting the text and source position of an ast.Node.
// Only the ast.NodeKinds that are relevant are covered.
func getTextAndPos(node ast.Node, source []byte) (text string, start, end int) {
	start, end = getPos(node)
	switch node.Kind() {
	case ast.KindFencedCodeBlock, ast.KindCodeBlock:
		l := node.Lines().Len()
		var buf bytes.Buffer
		for i := 0; i < l; i++ {
			line := node.Lines().At(i)
			buf.Write(line.Value(source))
		}
		text = buf.String()

	case ast.KindCodeSpan:
		n := node.(*ast.CodeSpan)
		text = string(n.Text(source))

	case ast.KindHeading:
		n := node.(*ast.Heading)
		text = sanitizeDescText(string(n.Text(source)))

	case ast.KindText:
		n := node.(*ast.Text)
		text = sanitizeDescText(string(n.Text(source)))

	case ast.KindParagraph:
		// we only want the last line of the paragraph to use as description
		l := node.Lines().Len()
		line := node.Lines().At(l - 1)
		// since we do not use the full paragraph we need to make sure to overwrite position data with the data of the line we are using
		start, end = line.Start, line.Stop
		text = sanitizeDescText(string(line.Value(source)))

	}
	return
}

func sanitizeDescText(text string) string {
	return strings.TrimRight(text, ":")
}

// getSourcePosition returns the Position (line based) of the code section based on
// the byte slice indexes of start and end of the code section in the source.
func getSourcePosition(start, end int, lineBreaks []int) Position {
	sLine, _ := slices.BinarySearch[[]int](lineBreaks, start)
	eLine, _ := slices.BinarySearch[[]int](lineBreaks, end)
	return Position{sLine, eLine}
}

// getFencedCodeBlockCode returns the Code representation of a code section defined
// with ``` and normally with a language.
func getFencedCodeBlockCode(node ast.Node, source []byte, lb []int) *Code {
	description, start, _ := getTextAndPos(node.PreviousSibling(), source)
	n := node.(*ast.FencedCodeBlock)
	lang := string(n.Info.Segment.Value(source))
	text, _, end := getTextAndPos(node, source)
	p := getSourcePosition(start, end, lb)
	return &Code{Description: description, Kind: BlockWithLang, Position: p, Language: lang, Text: text}
}

// getCodeBlockCode returns the Code representation of an indented code section.
// This never has a language defined.
func getCodeBlockCode(node ast.Node, source []byte, lb []int) *Code {
	description, start, _ := getTextAndPos(node.PreviousSibling(), source)
	text, _, end := getTextAndPos(node, source)
	// for some reason the newline character at the end of the code block still belongs
	// to the code block so we ignore/correct that for the position data
	end -= 1
	p := getSourcePosition(start, end, lb)
	return &Code{Description: description, Kind: Block, Position: p, Language: "", Text: text}
}

// getCodeSpanCode returns the Code representation of inline code sections.
// The Code snippet will only have a description if it is mentioned in a specific format.
func getCodeSpanCode(node ast.Node, source []byte, lb []int) *Code {
	description := ""
	// Finding a description will only work in some very specific situations.
	// Inline code sections must follow a pattern for us to know what to use as description.
	if node.Parent().Kind() == ast.KindTextBlock && node.Parent().Parent().Kind() == ast.KindListItem {
		// we are looking for code sections like this
		// * some description of the code: `the code`
		pt, _, _ := getTextAndPos(node.PreviousSibling(), source)
		if r := []rune(pt); string(r[len(r)-2:]) == ": " {
			Log.Debug("found an inline code section that has a description before it\n")
			description = string(r[:len(r)-2])
		}
	}
	text, start, end := getTextAndPos(node, source)
	p := getSourcePosition(start, end, lb)
	return &Code{Description: description, Kind: Inline, Position: p, Language: "", Text: text}
}
